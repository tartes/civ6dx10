// Exclude rarely-used stuff from Windows headers           
#define WIN32_LEAN_AND_MEAN  
#define printf printf_orig
#include <windows.h>
#define D3D11CreateDevice D3D11CreateDevice_orig
#define CINTERFACE
#define D3D11_NO_HELPERS
#include <d3d11.h>
#include <d3dcompiler.h>
#undef D3D11CreateDevice

#include <unordered_map>
#include <unordered_set>
#include <map>
#include <thread>
#include <string>
using namespace std::string_literals;
#include <fstream>
#include <functional>

#include <easyhook.h>
#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Multiline_Output.H>
#include <FL/Fl_Value_Slider.H>
#include <FL/Fl_Check_Browser.H>
#include <FL/Fl_Tabs.H>
#include <FL/Fl_Group.H>
#include <FL/Fl_Button.H>

#include "sha256.h"
#include "SimpleIni.h"
#undef printf

#define DEFINE_GUID(name, l, w1, w2, b1, b2, b3, b4, b5, b6, b7, b8) \
         const GUID name \
                = { l, w1, w2, { b1, b2,  b3,  b4,  b5,  b6,  b7,  b8 } }
DEFINE_GUID(IID_IDXGIFactory, 0x7b7166ec, 0x21c7, 0x44ae, 0xb2, 0x1a, 0xc9, 0xae, 0x32, 0x1a, 0xe3, 0x69);
DEFINE_GUID(IID_IDXGIDevice, 0x54ec77fa, 0x1377, 0x44e6, 0x8c, 0x32, 0x88, 0xfd, 0x5f, 0x44, 0xc8, 0x4c);

HMODULE dxLib = nullptr;
PFN_D3D11_CREATE_DEVICE orig_D3D11CreateDevice = nullptr;
pD3DDisassemble ptrD3DDisassemble;


////////////////////////////////////////// Configuration

class Config
{
public:
	static Config &instance()
	{
		static Config instance;
		return instance;
	}
	
	std::unordered_map<std::string, bool> vertexShadersInitialEnabled;
	std::unordered_map<std::string, bool> pixelShadersInitialEnabled;

	bool guiEnabled;
	bool disableTexFiltering;
	bool dumpShaders;

private:
	Config()
	{
		CSimpleIniA ini;
		if (ini.LoadFile("shaders/shaders.ini") >= 0)
		{
			decltype(ini)::TNamesDepend keys;
			if (ini.GetAllKeys("vertex", keys))
				for (auto &k : keys)
					vertexShadersInitialEnabled[k.pItem] = ini.GetBoolValue("vertex", k.pItem);

			keys.clear();

			if (ini.GetAllKeys("pixel", keys))
				for (auto &k : keys)
					pixelShadersInitialEnabled[k.pItem] = ini.GetBoolValue("pixel", k.pItem);
		}

		ini.LoadFile("dxconfig.ini");

		guiEnabled = ini.GetBoolValue("", "GUI", false);
		disableTexFiltering = ini.GetBoolValue("", "DisableTextureFiltering", false);
		dumpShaders = ini.GetBoolValue("", "DumpShaders", false);
	};
};

////////////////////////////////////////// Utility functions

void printf(char *format, ...)
{
	va_list va;
	va_start(va, format);

	char buf[1024 * 4];
	int len = wvsprintfA(buf, format, va);

	/*auto log = std::fstream("d:\\Programs\\Sid Meier's Civilization VI\\Base\\Binaries\\Win64Steam\\log.txt", std::ios_base::out | std::ios_base::ate);
	log << buf;
	log.flush();
	log.close();*/

	if (!Config::instance().guiEnabled)
		return;

	HANDLE con = GetStdHandle(STD_OUTPUT_HANDLE);

	if (!con)
	{
		AllocConsole();
		con = GetStdHandle(STD_OUTPUT_HANDLE);
	}

	if (con)
	{
		DWORD w;
		WriteConsoleA(con, buf, len, &w, NULL);
	}
}

std::unordered_set<void*> hookedAddr;
template<class T>
void Hook(T& ptr, T hook, T& orig)
{
	/*if (ptr == hook)
		return;

	orig = ptr;
	DWORD oldProtect;
	VirtualProtect(&ptr, 8, PAGE_READWRITE, &oldProtect);
	ptr = hook;
	VirtualProtect(&ptr, 8, oldProtect, &oldProtect);*/
	if (hookedAddr.find(ptr) != hookedAddr.end())
		return;
	hookedAddr.insert(ptr);

	orig = ptr;
	HOOK_TRACE_INFO hHook = {0};
	LhInstallHook(ptr, hook, nullptr, &hHook);
	ULONG threads[1] = {0};
	LhSetExclusiveACL(threads, 0, &hHook);
}

////////////////////////////////////////// CreateSamplerState hook

std::unordered_map<D3D11_FILTER, char*> filterTypesStr = 
{
        {D3D11_FILTER_MIN_MAG_MIP_POINT, "D3D11_FILTER_MIN_MAG_MIP_POINT"},
        {D3D11_FILTER_MIN_MAG_POINT_MIP_LINEAR, "D3D11_FILTER_MIN_MAG_POINT_MIP_LINEAR"},
        {D3D11_FILTER_MIN_POINT_MAG_LINEAR_MIP_POINT, "D3D11_FILTER_MIN_POINT_MAG_LINEAR_MIP_POINT"},
        {D3D11_FILTER_MIN_POINT_MAG_MIP_LINEAR, "D3D11_FILTER_MIN_POINT_MAG_MIP_LINEAR"},
        {D3D11_FILTER_MIN_LINEAR_MAG_MIP_POINT, "D3D11_FILTER_MIN_LINEAR_MAG_MIP_POINT"},
        {D3D11_FILTER_MIN_LINEAR_MAG_POINT_MIP_LINEAR, "D3D11_FILTER_MIN_LINEAR_MAG_POINT_MIP_LINEAR"},
        {D3D11_FILTER_MIN_MAG_LINEAR_MIP_POINT, "D3D11_FILTER_MIN_MAG_LINEAR_MIP_POINT"},
        {D3D11_FILTER_MIN_MAG_MIP_LINEAR, "D3D11_FILTER_MIN_MAG_MIP_LINEAR"},
        {D3D11_FILTER_ANISOTROPIC, "D3D11_FILTER_ANISOTROPIC"},
        {D3D11_FILTER_COMPARISON_MIN_MAG_MIP_POINT, "D3D11_FILTER_COMPARISON_MIN_MAG_MIP_POINT"},
        {D3D11_FILTER_COMPARISON_MIN_MAG_POINT_MIP_LINEAR, "D3D11_FILTER_COMPARISON_MIN_MAG_POINT_MIP_LINEAR"},
        {D3D11_FILTER_COMPARISON_MIN_POINT_MAG_LINEAR_MIP_POINT, "D3D11_FILTER_COMPARISON_MIN_POINT_MAG_LINEAR_MIP_POINT"},
        {D3D11_FILTER_COMPARISON_MIN_POINT_MAG_MIP_LINEAR, "D3D11_FILTER_COMPARISON_MIN_POINT_MAG_MIP_LINEAR"},
        {D3D11_FILTER_COMPARISON_MIN_LINEAR_MAG_MIP_POINT, "D3D11_FILTER_COMPARISON_MIN_LINEAR_MAG_MIP_POINT"},
        {D3D11_FILTER_COMPARISON_MIN_LINEAR_MAG_POINT_MIP_LINEAR, "D3D11_FILTER_COMPARISON_MIN_LINEAR_MAG_POINT_MIP_LINEAR"},
        {D3D11_FILTER_COMPARISON_MIN_MAG_LINEAR_MIP_POINT, "D3D11_FILTER_COMPARISON_MIN_MAG_LINEAR_MIP_POINT"},
        {D3D11_FILTER_COMPARISON_MIN_MAG_MIP_LINEAR, "D3D11_FILTER_COMPARISON_MIN_MAG_MIP_LINEAR"},
        {D3D11_FILTER_COMPARISON_ANISOTROPIC, "D3D11_FILTER_COMPARISON_ANISOTROPIC"},
};

decltype(ID3D11DeviceVtbl::CreateSamplerState) orig_CreateSamplerState;
HRESULT STDMETHODCALLTYPE ourCreateSamplerState(
	ID3D11Device * This,
	_In_  const D3D11_SAMPLER_DESC *pSamplerDesc,
	_Out_opt_  ID3D11SamplerState **ppSamplerState)
{
	printf("ourCreateSamplerState called!! caller=%016X dev=%016X filter=%s\n", _ReturnAddress(), This, filterTypesStr[pSamplerDesc->Filter]);

	D3D11_SAMPLER_DESC newDesc;
	memcpy(&newDesc, pSamplerDesc, sizeof(D3D11_SAMPLER_DESC));

	if (D3D11_DECODE_IS_COMPARISON_FILTER(pSamplerDesc->Filter))
	{
		newDesc.Filter = D3D11_FILTER_COMPARISON_MIN_MAG_MIP_POINT;
		//newDesc.MipLODBias = 1.0;
		printf("Changed to point\n");
	}
	else
	{
		newDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_POINT;
		//newDesc.MipLODBias = 1.0;
		//newDesc.MaxLOD = 0.0f;
		printf("Changed to point\n");
	}

	return orig_CreateSamplerState(This, &newDesc, ppSamplerState);
}

////////////////////////////////////////// Drawing hooks

decltype(ID3D11DeviceContextVtbl::Draw) orig_Draw;
decltype(ID3D11DeviceContextVtbl::DrawAuto) orig_DrawAuto;

decltype(ID3D11DeviceContextVtbl::DrawIndexed) orig_DrawIndexed;
decltype(ID3D11DeviceContextVtbl::DrawIndexedInstanced) orig_DrawIndexedInstanced;
decltype(ID3D11DeviceContextVtbl::DrawIndexedInstancedIndirect) orig_DrawIndexedInstancedIndirect;

decltype(ID3D11DeviceContextVtbl::DrawInstanced) orig_DrawInstanced;
decltype(ID3D11DeviceContextVtbl::DrawInstancedIndirect) orig_DrawInstancedIndirect;
decltype(ID3D11DeviceContextVtbl::Dispatch) orig_Dispatch;
decltype(ID3D11DeviceContextVtbl::DispatchIndirect) orig_DispatchIndirect;

volatile LONG64 drawCallsCount = 0;
volatile LONG64 dispatchCallsCount = 0;
volatile LONG64 framesCount = 0;

struct ShaderDesc
{
	bool IsEnabled;
	LONG64 ExecCount;
	int Index;
	std::string Hash;

	void *Replacement;
};

std::unordered_map<ID3D11ComputeShader*, ShaderDesc> computeShaders;
Fl_Check_Browser *computeShadersEnabled_cb;

std::unordered_map<ID3D11VertexShader*, ShaderDesc> vertexShaders;
Fl_Check_Browser *vertexShadersEnabled_cb;

std::unordered_map<ID3D11PixelShader*, ShaderDesc> pixelShaders;
Fl_Check_Browser *pixelShadersEnabled_cb;

struct TextureSplit
{
	ID3D11Texture2D *addTexture;
	ID3D11ShaderResourceView *addTextureView;
};

std::unordered_map<ID3D11Texture2D*, TextureSplit> textureSplits;
std::unordered_map<ID3D11ShaderResourceView*, TextureSplit*> textureSplitsViews;

void ProcessDraw(ID3D11DeviceContext * This, const std::function<void()> &draw)
{
	//draw();
	//return;

	ID3D11VertexShader *vs = nullptr;
	ID3D11PixelShader *ps = nullptr;

	if(This->lpVtbl->VSGetShader)
		This->lpVtbl->VSGetShader(This, &vs, NULL, NULL);

	if(This->lpVtbl->PSGetShader)
		This->lpVtbl->PSGetShader(This, &ps, NULL, NULL);

	bool enabled = true;
	if (vs && !vertexShaders[vs].IsEnabled)
		enabled = false;
	if(enabled && ps && !pixelShaders[ps].IsEnabled)
		enabled = false;
	
	if (vs)	vertexShaders[vs].ExecCount++;		
	if (ps)	pixelShaders[ps].ExecCount++;

	if (enabled)
	{
		draw();
	}

	if (vs) vs->lpVtbl->Release(vs);
	if (ps) ps->lpVtbl->Release(ps);
}

void STDMETHODCALLTYPE ourDraw(
	ID3D11DeviceContext * This,
	_In_  UINT VertexCount,
	_In_  UINT StartVertexLocation)
{
	_InterlockedIncrement64(&drawCallsCount);
	//printf("Draw, caller=%016X context=%016X\n", _ReturnAddress(), This);
	ProcessDraw(This, 
		std::bind(orig_Draw, This, VertexCount, StartVertexLocation)
	);
}

void STDMETHODCALLTYPE ourDrawAuto(ID3D11DeviceContext * This)
{
	_InterlockedIncrement64(&drawCallsCount);
	//printf("DrawAuto, caller=%016X context=%016X\n", _ReturnAddress(), This);
	ProcessDraw(This,
		std::bind(orig_DrawAuto, This)
	);
}

void STDMETHODCALLTYPE ourDrawIndexed(
	ID3D11DeviceContext * This,
	_In_  UINT IndexCount,
	_In_  UINT StartIndexLocation,
	_In_  INT BaseVertexLocation)
{
	_InterlockedIncrement64(&drawCallsCount);
	//printf("DrawIndexed, caller=%016X context=%016X\n", _ReturnAddress(), This);
	ProcessDraw(This,
		std::bind(orig_DrawIndexed, This, IndexCount, StartIndexLocation, BaseVertexLocation)
	);
}


void STDMETHODCALLTYPE ourDrawIndexedInstanced(
	ID3D11DeviceContext * This,
	_In_  UINT IndexCountPerInstance,
	_In_  UINT InstanceCount,
	_In_  UINT StartIndexLocation,
	_In_  INT BaseVertexLocation,
	_In_  UINT StartInstanceLocation)
{
	_InterlockedIncrement64(&drawCallsCount);
	//printf("DrawIndexedInstanced, caller=%016X context=%016X\n", _ReturnAddress(), This);
	ProcessDraw(This,
		std::bind(orig_DrawIndexedInstanced, This, IndexCountPerInstance, InstanceCount, StartIndexLocation, BaseVertexLocation, StartInstanceLocation)
	);
}

void STDMETHODCALLTYPE ourDrawIndexedInstancedIndirect(
	ID3D11DeviceContext * This,
	_In_  ID3D11Buffer *pBufferForArgs,
	_In_  UINT AlignedByteOffsetForArgs)
{
	_InterlockedIncrement64(&drawCallsCount);
	//printf("DrawIndexedInstancedIndirect, caller=%016X context=%016X\n", _ReturnAddress(), This);
//	if(!pBufferForArgs)
//		orig_DrawIndexedInstancedIndirect(This, pBufferForArgs, AlignedByteOffsetForArgs);
//	else
	ProcessDraw(This,
		std::bind(orig_DrawIndexedInstancedIndirect, This, pBufferForArgs, AlignedByteOffsetForArgs)
	);
}

void STDMETHODCALLTYPE ourDrawInstanced(
	ID3D11DeviceContext * This,
	_In_  UINT VertexCountPerInstance,
	_In_  UINT InstanceCount,
	_In_  UINT StartVertexLocation,
	_In_  UINT StartInstanceLocation)
{
	_InterlockedIncrement64(&drawCallsCount);
	//printf("DrawInstanced, caller=%016X context=%016X\n", _ReturnAddress(), This);
	ProcessDraw(This,
		std::bind(orig_DrawInstanced, This, VertexCountPerInstance, InstanceCount, StartVertexLocation, StartInstanceLocation)
	);
}

void STDMETHODCALLTYPE ourDrawInstancedIndirect(
	ID3D11DeviceContext * This,
	_In_  ID3D11Buffer *pBufferForArgs,
	_In_  UINT AlignedByteOffsetForArgs)
{
	_InterlockedIncrement64(&drawCallsCount);
	//printf("DrawInstancedIndirect, caller=%016X context=%016X\n", _ReturnAddress(), This);
	ProcessDraw(This,
		std::bind(orig_DrawInstancedIndirect, This, pBufferForArgs, AlignedByteOffsetForArgs)
	);
}

void STDMETHODCALLTYPE ourDispatch(
	ID3D11DeviceContext * This,
	_In_  UINT ThreadGroupCountX,
	_In_  UINT ThreadGroupCountY,
	_In_  UINT ThreadGroupCountZ)
{
	_InterlockedIncrement64(&dispatchCallsCount);
	//printf("Dispatch, caller=%016X context=%016X\n", _ReturnAddress(), This);
	
	ID3D11ComputeShader *shader = nullptr;
	This->lpVtbl->CSGetShader(This, &shader, NULL, NULL);
	if (shader && computeShaders[shader].IsEnabled)
	{
		orig_Dispatch(This, ThreadGroupCountX, ThreadGroupCountY, ThreadGroupCountZ);
		computeShaders[shader].ExecCount++;
	}
	if (shader) shader->lpVtbl->Release(shader);
}

void STDMETHODCALLTYPE ourDispatchIndirect(
	ID3D11DeviceContext * This,
	_In_  ID3D11Buffer *pBufferForArgs,
	_In_  UINT AlignedByteOffsetForArgs)
{
	_InterlockedIncrement64(&dispatchCallsCount);
	//printf("DispatchIndirect, caller=%016X context=%016X\n", _ReturnAddress(), This);
	
	ID3D11ComputeShader *shader = nullptr;
	This->lpVtbl->CSGetShader(This, &shader, NULL, NULL);
	if (shader && computeShaders[shader].IsEnabled)
	{
		orig_DispatchIndirect(This, pBufferForArgs, AlignedByteOffsetForArgs);
		computeShaders[shader].ExecCount++;
	}
	if (shader) shader->lpVtbl->Release(shader);
}

////////////////////////////////////////// Shader creation hooks

void WriteBinToFile(const std::string& file, const void *data, SIZE_T size)
{
	auto log = std::ofstream(file, std::ios::binary);
	log.write((char*)data, size);
	log.flush();
	log.close();
}

void WriteBlobToFile(const std::string& file, ID3DBlob *blob)
{
	WriteBinToFile(file, blob->lpVtbl->GetBufferPointer(blob), blob->lpVtbl->GetBufferSize(blob));
}

decltype(ID3D11DeviceVtbl::CreateComputeShader) orig_CreateComputeShader;
decltype(ID3D11DeviceVtbl::CreateVertexShader) orig_CreateVertexShader;
decltype(ID3D11DeviceVtbl::CreateHullShader) orig_CreateHullShader;
decltype(ID3D11DeviceVtbl::CreateDomainShader) orig_CreateDomainShader;
decltype(ID3D11DeviceVtbl::CreateGeometryShader) orig_CreateGeometryShader;
decltype(ID3D11DeviceVtbl::CreateGeometryShaderWithStreamOutput) orig_CreateGeometryShaderWithStreamOutput;
decltype(ID3D11DeviceVtbl::CreatePixelShader) orig_CreatePixelShader;

std::string ShaderHash(const void *pShaderBytecode, SIZE_T BytecodeLength)
{
	SHA256_CTX sha;
	uint32_t hash[8];
	SHA256_Init(&sha);
	SHA256_Update(&sha, pShaderBytecode, BytecodeLength);
	SHA256_Final((uint8_t*)hash, &sha);

	char res[9];
	snprintf(res, 9, "%08X", hash[0]);	
	return std::string(res);
}

void ProcessCreateShader(const std::string &ext,
	const void *pShaderBytecode, SIZE_T BytecodeLength)
{
	if (!Config::instance().dumpShaders)
		return;

	auto hash = ShaderHash(pShaderBytecode, BytecodeLength);

	auto dir = "./shaders/"s + ext + "/";
	auto binfName = hash + "." + ext + "bin";

	if (std::ifstream(dir + binfName).good())
		return;

	/*int tries = 1;
	while (std::ifstream(dir + binfName).good())
	{
		printf("%s: ALREADY EXISTS!!!\n", binfName.c_str());
		binfName = hash + "_" + std::to_string(tries++) + "." + ext + "bin";
	}*/

	WriteBinToFile(dir + binfName, pShaderBytecode, BytecodeLength);
	printf("%s shader created, bin saved as %s\n", ext.c_str(), binfName.c_str());

	ID3DBlob *text;
	if (SUCCEEDED(ptrD3DDisassemble(pShaderBytecode, BytecodeLength, 0, NULL, &text)))
	{
		auto fName = hash + std::string(".") + ext;
		WriteBlobToFile(dir + fName, text);
		printf("Successfuly disassembled to %s\n", fName.c_str());
	}
	else
		printf("Disassemble failed");
}

template<class T>
HRESULT ProcessCreateShader(const std::string &ext, 
	std::unordered_map<T, ShaderDesc> &shaders, Fl_Check_Browser *browser,
	const void *pShaderBytecode, SIZE_T BytecodeLength, 
	const std::function<HRESULT(const void*, SIZE_T, T*)> createFunc, 
	T* ppShader)
{
	auto hash = ShaderHash(pShaderBytecode, BytecodeLength);
	auto dir = "./shaders/"s + ext + "/";
	auto binfName = hash + ".cso";

	HRESULT hr;
	if (std::ifstream &f = std::ifstream(dir + binfName, std::ios::binary))
	{
		f.seekg(0, std::ios::end);
		auto s = f.tellg();
		f.seekg(0, std::ios::beg);
		auto data = new char[s];
		f.read(data, s);

		hr = createFunc(data, s, ppShader);
		delete[] data;

		if (SUCCEEDED(hr))
			printf("Shader %s%s replaced\n", ext.c_str(), hash.c_str());
		else
			printf("Shader %s%s replace error=%08X!!!\n", ext.c_str(), hash.c_str(), hr);
	}
	else
	{
		//*ppShader = nullptr;
		//return S_OK;
		hr = createFunc(pShaderBytecode, BytecodeLength, ppShader);
	}
		
	
	if (FAILED(hr))
	{
		printf("Failed create shader %s%s, code %08X\n", ext.c_str(), hash.c_str(), hr);
	}

	if (ppShader)
	{
		auto &desc = shaders[*ppShader];

		desc.ExecCount = 0;
		desc.Replacement = nullptr;
		desc.Hash = hash;

		if (ext == "vs")
		{
			auto &enabled = Config::instance().vertexShadersInitialEnabled.find(hash);
			if(enabled != Config::instance().vertexShadersInitialEnabled.end())
				desc.IsEnabled = enabled->second;
		}
		else if (ext == "ps")
		{
			auto &enabled = Config::instance().pixelShadersInitialEnabled.find(hash);
			if (enabled != Config::instance().pixelShadersInitialEnabled.end())
				desc.IsEnabled = enabled->second;
		}
		else
			desc.IsEnabled = true;

		if (Config::instance().guiEnabled)
		{
			char title[30];

			snprintf(title, 30, "%-29s", hash.c_str());

			Fl::lock();
			desc.Index = browser->add(title, desc.IsEnabled ? 1 : 0);
			Fl::unlock();
			Fl::awake();
		}
	}

	ProcessCreateShader(ext, pShaderBytecode, BytecodeLength);
	
	return hr;
}

HRESULT STDMETHODCALLTYPE ourCreateComputeShader(
	ID3D11Device * This,
	_In_reads_(BytecodeLength)  const void *pShaderBytecode,
	_In_  SIZE_T BytecodeLength,
	_In_opt_  ID3D11ClassLinkage *pClassLinkage,
	_Out_opt_  ID3D11ComputeShader **ppComputeShader)
{
	//return S_OK;
	return ProcessCreateShader<ID3D11ComputeShader *>("cs",
		computeShaders, computeShadersEnabled_cb,
		pShaderBytecode, BytecodeLength,
		std::bind(orig_CreateComputeShader, This, std::placeholders::_1, std::placeholders::_2, pClassLinkage, std::placeholders::_3),
		ppComputeShader);
}

HRESULT STDMETHODCALLTYPE ourCreateVertexShader(
	ID3D11Device * This,
	_In_reads_(BytecodeLength)  const void *pShaderBytecode,
	_In_  SIZE_T BytecodeLength,
	_In_opt_  ID3D11ClassLinkage *pClassLinkage,
	_Out_opt_  ID3D11VertexShader **ppVertexShader)
{
	return ProcessCreateShader<ID3D11VertexShader *>("vs",
		vertexShaders, vertexShadersEnabled_cb,
		pShaderBytecode, BytecodeLength,
		std::bind(orig_CreateVertexShader, This, std::placeholders::_1, std::placeholders::_2, pClassLinkage, std::placeholders::_3),
		ppVertexShader);
}

HRESULT STDMETHODCALLTYPE ourCreateHullShader(
	ID3D11Device * This,
	_In_reads_(BytecodeLength)  const void *pShaderBytecode,
	_In_  SIZE_T BytecodeLength,
	_In_opt_  ID3D11ClassLinkage *pClassLinkage,
	_Out_opt_  ID3D11HullShader **ppHullShader)
{
	auto hr = orig_CreateHullShader(This, pShaderBytecode, BytecodeLength, pClassLinkage, ppHullShader);
	ProcessCreateShader("hs", pShaderBytecode, BytecodeLength);
	return hr;
}

HRESULT STDMETHODCALLTYPE ourCreateDomainShader(
	ID3D11Device * This,
	_In_reads_(BytecodeLength)  const void *pShaderBytecode,
	_In_  SIZE_T BytecodeLength,
	_In_opt_  ID3D11ClassLinkage *pClassLinkage,
	_Out_opt_  ID3D11DomainShader **ppDomainShader)
{
	auto hr = orig_CreateDomainShader(This, pShaderBytecode, BytecodeLength, pClassLinkage, ppDomainShader);
	ProcessCreateShader("ds", pShaderBytecode, BytecodeLength);
	return hr;
}

HRESULT STDMETHODCALLTYPE ourCreateGeometryShader(
	ID3D11Device * This,
	_In_reads_(BytecodeLength)  const void *pShaderBytecode,
	_In_  SIZE_T BytecodeLength,
	_In_opt_  ID3D11ClassLinkage *pClassLinkage,
	_Out_opt_  ID3D11GeometryShader **ppGeometryShader)
{
	auto hr = orig_CreateGeometryShader(This, pShaderBytecode, BytecodeLength, pClassLinkage, ppGeometryShader);
	ProcessCreateShader("gs", pShaderBytecode, BytecodeLength);
	return hr;
}

HRESULT STDMETHODCALLTYPE ourCreateGeometryShaderWithStreamOutput(
	ID3D11Device * This,
	_In_reads_(BytecodeLength)  const void *pShaderBytecode,
	_In_  SIZE_T BytecodeLength,
	_In_reads_opt_(NumEntries)  const D3D11_SO_DECLARATION_ENTRY *pSODeclaration,
	_In_range_(0, D3D11_SO_STREAM_COUNT * D3D11_SO_OUTPUT_COMPONENT_COUNT)  UINT NumEntries,
	_In_reads_opt_(NumStrides)  const UINT *pBufferStrides,
	_In_range_(0, D3D11_SO_BUFFER_SLOT_COUNT)  UINT NumStrides,
	_In_  UINT RasterizedStream,
	_In_opt_  ID3D11ClassLinkage *pClassLinkage,
	_Out_opt_  ID3D11GeometryShader **ppGeometryShader)
{
	auto hr = orig_CreateGeometryShaderWithStreamOutput(This, pShaderBytecode, BytecodeLength,
		pSODeclaration, NumEntries, pBufferStrides, NumStrides, RasterizedStream,
		pClassLinkage, ppGeometryShader);
	ProcessCreateShader("gsso", pShaderBytecode, BytecodeLength);
	return hr;
}

HRESULT STDMETHODCALLTYPE ourCreatePixelShader(
	ID3D11Device * This,
	_In_reads_(BytecodeLength)  const void *pShaderBytecode,
	_In_  SIZE_T BytecodeLength,
	_In_opt_  ID3D11ClassLinkage *pClassLinkage,
	_Out_opt_  ID3D11PixelShader **ppPixelShader)
{
	return ProcessCreateShader<ID3D11PixelShader *>("ps", 
		pixelShaders, pixelShadersEnabled_cb,
		pShaderBytecode, BytecodeLength, 
		std::bind(orig_CreatePixelShader, This, std::placeholders::_1, std::placeholders::_2, pClassLinkage, std::placeholders::_3),
		ppPixelShader);
}

////////////////////////////////////////// Big texture arrays splitting

decltype(ID3D11DeviceVtbl::CreateTexture2D) orig_CreateTexture2D;
HRESULT STDMETHODCALLTYPE ourCreateTexture2D(
	ID3D11Device * This,
	_In_  const D3D11_TEXTURE2D_DESC *pDesc,
	_In_reads_opt_(_Inexpressible_(pDesc->MipLevels * pDesc->ArraySize))  const D3D11_SUBRESOURCE_DATA *pInitialData,
	_Out_opt_  ID3D11Texture2D **ppTexture2D)
{
	D3D11_TEXTURE2D_DESC newDesc;
	memcpy(&newDesc, pDesc, sizeof(D3D11_TEXTURE2D_DESC));
	newDesc.BindFlags &= ~D3D11_BIND_UNORDERED_ACCESS;
	bool split = false;
	if (newDesc.ArraySize > 512)
	{
		newDesc.ArraySize = 512;
		split = true;
	}
	
	auto hr = orig_CreateTexture2D(This, &newDesc, pInitialData, ppTexture2D);
	if (FAILED(hr))
	{
		DWORD addr = 0;
		LhBarrierGetReturnAddress((PVOID*)&addr);
		printf("CreateTexture2D failed!! caller=%016X hr=%08X width=%i height=%i mips=%i array=%i format=%08X samplec=%i sampleq=%i usage=%08X bind=%08X cpu=%08X flags=%08X\n", 
			addr, hr,
			pDesc->Width, pDesc->Height, pDesc->MipLevels, pDesc->ArraySize,
			pDesc->Format, pDesc->SampleDesc.Count, pDesc->SampleDesc.Quality,
			pDesc->Usage, pDesc->BindFlags, pDesc->CPUAccessFlags, pDesc->MiscFlags);

		if (hr == DXGI_ERROR_DEVICE_REMOVED)
		{
			hr = This->lpVtbl->GetDeviceRemovedReason(This);
			printf("DEVICE REMOVED!!!!!! hr=%08X\n", hr);
		}
	}

	if (split)
	{
		auto newInitialData = pInitialData;
		if (newInitialData)
			newInitialData += newDesc.ArraySize * pDesc->MipLevels;

		newDesc.ArraySize = pDesc->ArraySize - newDesc.ArraySize;

		orig_CreateTexture2D(This, &newDesc, newInitialData, &textureSplits[*ppTexture2D].addTexture);

		printf("Big tex split, arraysize=%i initial=%016X\n", pDesc->ArraySize, pInitialData);
	}

	return hr;
}

decltype(ID3D11DeviceVtbl::CreateShaderResourceView) orig_CreateShaderResourceView;
HRESULT STDMETHODCALLTYPE ourCreateShaderResourceView(
	ID3D11Device * This,
	_In_  ID3D11Resource *pResource,
	_In_opt_  const D3D11_SHADER_RESOURCE_VIEW_DESC *pDesc,
	_Out_opt_  ID3D11ShaderResourceView **ppSRView)
{
	HRESULT hr;
	auto tex = reinterpret_cast<ID3D11Texture2D*>(pResource);
	auto &i = textureSplits.find(tex);
	if (i != textureSplits.end())
	{
		D3D11_SHADER_RESOURCE_VIEW_DESC newDesc;
		memcpy(&newDesc, pDesc, sizeof(D3D11_SHADER_RESOURCE_VIEW_DESC));

		newDesc.Texture2DArray.ArraySize = 512;
		hr = orig_CreateShaderResourceView(This, pResource, &newDesc, ppSRView);

		if (SUCCEEDED(hr))
		{
			newDesc.Texture2DArray.ArraySize = pDesc->Texture2DArray.ArraySize - 512;
			newDesc.Texture2DArray.FirstArraySlice = 0;
			orig_CreateShaderResourceView(This, reinterpret_cast<ID3D11Resource*>(i->second.addTexture), &newDesc, &i->second.addTextureView);
			textureSplitsViews[*ppSRView] = &i->second;
		}
		else
		{
			printf("Failed create ShaderResourceView for splitted texure!!\n");
		}
	}
	else
		hr = orig_CreateShaderResourceView(This, pResource, pDesc, ppSRView);

	return hr;
}

decltype(ID3D11DeviceContextVtbl::PSSetShaderResources) orig_PSSetShaderResources;
void STDMETHODCALLTYPE ourPSSetShaderResources(
	ID3D11DeviceContext * This,
	_In_range_(0, D3D11_COMMONSHADER_INPUT_RESOURCE_SLOT_COUNT - 1)  UINT StartSlot,
	_In_range_(0, D3D11_COMMONSHADER_INPUT_RESOURCE_SLOT_COUNT - StartSlot)  UINT NumViews,
	_In_reads_opt_(NumViews)  ID3D11ShaderResourceView *const *ppShaderResourceViews)
{
	orig_PSSetShaderResources(This, StartSlot, NumViews, ppShaderResourceViews);

	for (int i = 0; i < NumViews; i++)
	{
		auto view = ppShaderResourceViews[i];
		auto &it = textureSplitsViews.find(view);
		if (it != textureSplitsViews.end())
		{
			orig_PSSetShaderResources(This, 99, 1, &it->second->addTextureView);
			break;
		}
	}
}

////////////////////////////////////////// Map fix

int mapFilter(unsigned int code, struct _EXCEPTION_POINTERS *ep) 
{
	printf("Map: caught exception %08X\n", code);

	return EXCEPTION_EXECUTE_HANDLER;
}
decltype(ID3D11DeviceContextVtbl::Map) orig_Map;
HRESULT checkedMap(ID3D11DeviceContext * This,
	_In_  ID3D11Resource *pResource,
	_In_  UINT Subresource,
	_In_  D3D11_MAP MapType,
	_In_  UINT MapFlags,
	_Out_  D3D11_MAPPED_SUBRESOURCE *pMappedResource)
{
	HRESULT hr;
	__try
	{
		hr = orig_Map(This, pResource, Subresource, MapType, MapFlags, pMappedResource);
	}
	__except (mapFilter(GetExceptionCode(), GetExceptionInformation()))
	{
		hr = E_INVALIDARG;
	}
	return hr;
}

HRESULT STDMETHODCALLTYPE ourMap(
	ID3D11DeviceContext * This,
	_In_  ID3D11Resource *pResource,
	_In_  UINT Subresource,
	_In_  D3D11_MAP MapType,
	_In_  UINT MapFlags,
	_Out_  D3D11_MAPPED_SUBRESOURCE *pMappedResource)
{
	HRESULT hr;
	hr = checkedMap(This, pResource, Subresource, MapType, MapFlags, pMappedResource);

	return hr;
}

//////////////////////////////////////////

void updateContextHooks(ID3D11DeviceContextVtbl *table)
{
	Hook(table->Draw, ourDraw, orig_Draw);
	Hook(table->DrawAuto, ourDrawAuto, orig_DrawAuto);
		 
	Hook(table->DrawIndexed, ourDrawIndexed, orig_DrawIndexed);
	Hook(table->DrawIndexedInstanced, ourDrawIndexedInstanced, orig_DrawIndexedInstanced);
	Hook(table->DrawIndexedInstancedIndirect, ourDrawIndexedInstancedIndirect, orig_DrawIndexedInstancedIndirect);
		 
	Hook(table->DrawInstanced, ourDrawInstanced, orig_DrawInstanced);
	Hook(table->DrawInstancedIndirect, ourDrawInstancedIndirect, orig_DrawInstancedIndirect);
		 
	Hook(table->Dispatch, ourDispatch, orig_Dispatch);
	Hook(table->DispatchIndirect, ourDispatchIndirect, orig_DispatchIndirect);
}

decltype(ID3D11DeviceVtbl::CreateDeferredContext) orig_CreateDeferredContext;
HRESULT STDMETHODCALLTYPE ourCreateDeferredContext(
	ID3D11Device * This,
	UINT ContextFlags,
	_Out_opt_  ID3D11DeviceContext **ppDeferredContext)
{
	auto res = orig_CreateDeferredContext(This, ContextFlags, ppDeferredContext);
	if (SUCCEEDED(res) && ppDeferredContext)
	{
		printf("Deffered context created, caller=%016X dev=%016X context=%016X\n", _ReturnAddress(), This, *ppDeferredContext);
		updateContextHooks((*ppDeferredContext)->lpVtbl);
	}

	return res;
}

decltype(IDXGISwapChainVtbl::Present) orig_Present;
HRESULT STDMETHODCALLTYPE ourPresent(
	IDXGISwapChain * This,
	UINT SyncInterval,
	UINT Flags)
{
	_InterlockedIncrement64(&framesCount);

	return orig_Present(This, SyncInterval, Flags);
}

decltype(IDXGIFactoryVtbl::CreateSwapChain) orig_CreateSwapChain;
HRESULT STDMETHODCALLTYPE ourCreateSwapChain(
	IDXGIFactory * This,
	_In_  IUnknown *pDevice,
	_In_  DXGI_SWAP_CHAIN_DESC *pDesc,
	_Out_  IDXGISwapChain **ppSwapChain)
{
	auto hr = orig_CreateSwapChain(This, pDevice, pDesc, ppSwapChain);
	if (SUCCEEDED(hr))
	{
		printf("Created SwapChain\n");
		Hook((*ppSwapChain)->lpVtbl->Present, ourPresent, orig_Present);
	}
	return hr;
}


decltype(ID3D11DeviceVtbl::CheckFeatureSupport) orig_CheckFeatureSupport;
HRESULT STDMETHODCALLTYPE ourCheckFeatureSupport(
	ID3D11Device * This,
	D3D11_FEATURE Feature,
	/* [annotation] */
	_Out_writes_bytes_(FeatureSupportDataSize)  void *pFeatureSupportData,
	UINT FeatureSupportDataSize)
{
	printf("CheckFeatureSupport dev=%016X feature=%i\n", This, Feature);

	return This->lpVtbl->CheckFeatureSupport(This, Feature, pFeatureSupportData, FeatureSupportDataSize);
}

decltype(ID3D11DeviceVtbl::GetFeatureLevel) orig_GetFeatureLevel;
D3D_FEATURE_LEVEL STDMETHODCALLTYPE ourGetFeatureLevel(
	ID3D11Device * This)
{
	return D3D_FEATURE_LEVEL_11_0;
}

////////////////////////////////////////// GUI Functions

IDXGIDevice *dxgiDev = nullptr;
void cb_SetPriority(Fl_Value_Slider *s, void *data)
{
	if (dxgiDev)
	{
		INT oldPri = 0, newPri = 0;
		dxgiDev->lpVtbl->GetGPUThreadPriority(dxgiDev, &oldPri);
		dxgiDev->lpVtbl->SetGPUThreadPriority(dxgiDev, INT(s->value()));
		dxgiDev->lpVtbl->GetGPUThreadPriority(dxgiDev, &newPri);
		printf("Device=%016X oldThreadPriority=%i newThreadPriority=%i\n", dxgiDev, oldPri, newPri);
	}
}

void cb_ShaderControl(Fl_Check_Browser *c, std::unordered_map<void*, ShaderDesc> *shaders)
{
	for (auto &item : (*shaders))
	{
		if (item.second.IsEnabled != (bool)c->checked(item.second.Index))
			printf("Shader%i: %s\n", item.second.Index - 1, c->checked(item.second.Index) ? "enable" : "disable");
		item.second.IsEnabled = (bool)c->checked(item.second.Index);
	}
}

/* Not implemented
void cb_ReloadShader(Fl_Button *b, int type)
{
	auto sel = pixelShadersEnabled_cb->value();
	if (sel == 0)
		return;

	for (auto &i = pixelShaders.begin(); i != pixelShaders.end(); i++)
	{
		if (i->second.Index == sel)
		{

			break;
		}
	}
}*/

void cb_LoadState(Fl_Button *b, int type)
{
	CSimpleIniA ini;
	if (ini.LoadFile("shaders/shaders.ini") < 0)
		return;

	std::unordered_map<void*, ShaderDesc> *shaders;
	char *section;
	Fl_Check_Browser *browser;

	if (type == 0)
	{
		shaders = reinterpret_cast<decltype(shaders)>(&vertexShaders);
		section = "vertex";
		browser = vertexShadersEnabled_cb;
	}
	else if (type == 1)
	{
		shaders = reinterpret_cast<decltype(shaders)>(&pixelShaders);
		section = "pixel";
		browser = pixelShadersEnabled_cb;
	}
	else
		return;

	Fl::lock();
	for (auto &i : *shaders)
	{
		auto res = ini.GetBoolValue(section, i.second.Hash.c_str());
		if (res)
		{
			i.second.IsEnabled = true;
			browser->checked(i.second.Index, 1);
		}
		else
		{
			i.second.IsEnabled = false;
			browser->checked(i.second.Index, 0);
		}
	}
	Fl::unlock();
	Fl::awake();
}

void cb_SaveState(Fl_Button *b, int type)
{
	CSimpleIniA ini;
	ini.LoadFile("shaders/shaders.ini");

	std::unordered_map<void*, ShaderDesc> *shaders;
	char *section;
	if (type == 0)
	{
		shaders = reinterpret_cast<decltype(shaders)>(&vertexShaders);
		section = "vertex";
	}
	else if (type == 1)
	{
		shaders = reinterpret_cast<decltype(shaders)>(&pixelShaders);
		section = "pixel";
	}

	for (auto &i : *shaders)
		ini.SetBoolValue(section, i.second.Hash.c_str(), i.second.IsEnabled);

	ini.SaveFile("shaders/shaders.ini");
}


volatile char guiCreated = 0;
void CreateGUI()
{
	if (_InterlockedCompareExchange8(&guiCreated, 1, 0) == 1)
		return;

	auto w = new Fl_Window(0, 0, 200, 400);
	w->resizable(w);

	/*auto priSlider = new Fl_Value_Slider(0,0, 200, 25);
	priSlider->align(FL_ALIGN_LEFT);
	priSlider->type(FL_HORIZONTAL);
	priSlider->range(-7, 7);
	priSlider->step(1);
	priSlider->callback((Fl_Callback*)cb_SetPriority);*/

	auto txt = new Fl_Multiline_Output(0, 25, 200, 25);


	auto tabs = new Fl_Tabs(0, 50, 200, 350);
	auto g = new Fl_Group(0, 70, 200, 330, "CS");
	computeShadersEnabled_cb = new Fl_Check_Browser(0, 70, 200, 310);
	computeShadersEnabled_cb->callback((Fl_Callback*)cb_ShaderControl, &computeShaders);
	computeShadersEnabled_cb->when(FL_WHEN_CHANGED);
	g->end();

	g = new Fl_Group(0, 70, 200, 330, "VS");
	vertexShadersEnabled_cb = new Fl_Check_Browser(0, 70, 200, 310);
	vertexShadersEnabled_cb->callback((Fl_Callback*)cb_ShaderControl, &vertexShaders);
	vertexShadersEnabled_cb->when(FL_WHEN_CHANGED);

	//Not implemented
	/*auto b = new Fl_Button(0, 380, 66, 20, "Reload");
	b->callback((Fl_Callback1*)cb_ReloadShader, 0L);*/
	auto b = new Fl_Button(67, 380, 66, 20, "Load state");
	b->callback((Fl_Callback1*)cb_LoadState, 0L);
	b = new Fl_Button(134, 380, 66, 20, "Save state");
	b->callback((Fl_Callback1*)cb_SaveState, 0L);

	g->hide();
	g->end();

	g = new Fl_Group(0, 70, 200, 330, "PS");
	pixelShadersEnabled_cb = new Fl_Check_Browser(0, 70, 200, 310);
	pixelShadersEnabled_cb->callback((Fl_Callback*)cb_ShaderControl, &pixelShaders);
	pixelShadersEnabled_cb->when(FL_WHEN_CHANGED);

	//Not implemented
	/*b = new Fl_Button(0, 380, 66, 20, "Reload");
	b->callback((Fl_Callback1*)cb_ReloadShader, 0L);*/
	b = new Fl_Button(67, 380, 66, 20, "Load state");
	b->callback((Fl_Callback1*)cb_LoadState, 1L);
	b = new Fl_Button(134, 380, 66, 20, "Save state");
	b->callback((Fl_Callback1*)cb_SaveState, 1L);

	g->hide();
	g->end();

	w->end();

	bool *locked = new bool(false);

	new std::thread([txt, w, locked]() {
		w->show();

		Fl::lock();
		*locked = true;
		Fl::run();
	});

	new std::thread([txt, locked]() {
		LONG64 currDraws = 0, currDispatch = 0, currFrames = 0;
		while (true)
		{
			auto ourDraws = drawCallsCount,
				ourDispatches = dispatchCallsCount,
				ourFrames = framesCount;

			char buf[200];
			sprintf(buf, "Dr/s=%i, di/s=%i, fps=%i\n",
				ourDraws - currDraws,
				ourDispatches - currDispatch,
				ourFrames - currFrames);
			if (*locked)
			{
				Fl::lock();

				txt->value(buf);

				for (auto &item : computeShaders)
				{
					auto str = computeShadersEnabled_cb->text(item.second.Index);
					if (str)
						snprintf(&str[10], 20, "%i", item.second.ExecCount);
				}
				computeShadersEnabled_cb->redraw();

				for (auto &item : vertexShaders)
				{
					auto str = vertexShadersEnabled_cb->text(item.second.Index);
					if (str)
						snprintf(&str[10], 20, "%i", item.second.ExecCount);
				}
				vertexShadersEnabled_cb->redraw();

				for (auto &item : pixelShaders)
				{
					auto str = pixelShadersEnabled_cb->text(item.second.Index);
					if (str)
						snprintf(&str[10], 20, "%i", item.second.ExecCount);
				}
				pixelShadersEnabled_cb->redraw();

				Fl::unlock();
				Fl::awake();
			}

			currDraws = ourDraws;
			currDispatch = ourDispatches;
			currFrames = ourFrames;
			std::this_thread::sleep_for(std::chrono::seconds(1));
		}
	});
}


////////////////////////////////////////// Hooked D3D11CreateDevice

std::unordered_map<D3D_DRIVER_TYPE, char *> driverTypesStr = {
	{D3D_DRIVER_TYPE_UNKNOWN, "D3D_DRIVER_TYPE_UNKNOWN"},
	{D3D_DRIVER_TYPE_HARDWARE, "D3D_DRIVER_TYPE_HARDWARE"},
	{D3D_DRIVER_TYPE_REFERENCE, "D3D_DRIVER_TYPE_REFERENCE"},
	{D3D_DRIVER_TYPE_NULL, "D3D_DRIVER_TYPE_NULL"},
	{D3D_DRIVER_TYPE_SOFTWARE, "D3D_DRIVER_TYPE_SOFTWARE"},
	{D3D_DRIVER_TYPE_WARP, "D3D_DRIVER_TYPE_WARP"},
};
EXTERN_C __declspec(dllexport) HRESULT APIENTRY D3D11CreateDevice(
	__in   IDXGIAdapter *pAdapter,
	__in   D3D_DRIVER_TYPE DriverType,
	__in   HMODULE Software,
	__in   UINT Flags,
	__in   const D3D_FEATURE_LEVEL *pFeatureLevels,
	__in   UINT FeatureLevels,
	__in   UINT SDKVersion,
	__out  ID3D11Device **ppDevice,
	__out  D3D_FEATURE_LEVEL *pFeatureLevel,
	__out  ID3D11DeviceContext **ppImmediateContext
)
{
	if (!dxLib)
	{
		dxLib = LoadLibrary(TEXT("c:\\windows\\System32\\d3d11.dll"));
		orig_D3D11CreateDevice = (PFN_D3D11_CREATE_DEVICE)GetProcAddress(dxLib, "D3D11CreateDevice");

		auto compilerLib = LoadLibrary(TEXT("d3dcompiler_47.dll"));
		ptrD3DDisassemble = (pD3DDisassemble)GetProcAddress(compilerLib, "D3DDisassemble");
	}
	
	printf("D3D11CreateDevice called, DriverType=%s\n", driverTypesStr[DriverType]);
	if (pAdapter)
	{
		IDXGIFactory *pFactory;
		if (SUCCEEDED(pAdapter->lpVtbl->GetParent(pAdapter, IID_IDXGIFactory, (void**)&pFactory)))
		{
			Hook(pFactory->lpVtbl->CreateSwapChain, ourCreateSwapChain, orig_CreateSwapChain);
		}
	}

	//DriverType = D3D_DRIVER_TYPE_WARP;
	//Flags |= D3D11_CREATE_DEVICE_PREVENT_INTERNAL_THREADING_OPTIMIZATIONS;
	//Flags |= D3D11_CREATE_DEVICE_DEBUG;
	D3D_FEATURE_LEVEL newFeatureLevels[1] = { D3D_FEATURE_LEVEL_10_1 };	
	HRESULT hr = orig_D3D11CreateDevice(pAdapter, DriverType, Software, Flags, newFeatureLevels, 1, SDKVersion,
		ppDevice, pFeatureLevel, ppImmediateContext);
	*pFeatureLevel = pFeatureLevels[0];


	/*char *ptr = (char*)0x0000000140149AF4;
	DWORD oldProtect;
	VirtualProtect(ptr, 8, PAGE_READWRITE, &oldProtect);
	*ptr = 8;
	VirtualProtect(ptr, 8, oldProtect, &oldProtect);
	
	ptr = (char*)0x00000001402FEE06;
	VirtualProtect(ptr, 8, PAGE_READWRITE, &oldProtect);
	*ptr = 8;
	VirtualProtect(ptr, 8, oldProtect, &oldProtect);*/

	if (SUCCEEDED(hr) && ppDevice /*&& ppImmediateContext*/)
	{
		//(*ppDevice)->lpVtbl->SetExceptionMode(*ppDevice, D3D11_RAISE_FLAG_DRIVER_INTERNAL_ERROR);

		printf("Device=%016X and context=%016X created, patching..\n", *ppDevice, ppImmediateContext ? *ppImmediateContext : 0);
		//Hook((*ppDevice)->lpVtbl->CheckFeatureSupport, ourCheckFeatureSupport, orig_CheckFeatureSupport);
		//Hook((*ppDevice)->lpVtbl->GetFeatureLevel, ourGetFeatureLevel, orig_GetFeatureLevel);

		if (Config::instance().disableTexFiltering)
			Hook((*ppDevice)->lpVtbl->CreateSamplerState, ourCreateSamplerState, orig_CreateSamplerState);

		Hook((*ppDevice)->lpVtbl->CreateComputeShader, ourCreateComputeShader, orig_CreateComputeShader);
		Hook((*ppDevice)->lpVtbl->CreateVertexShader, ourCreateVertexShader, orig_CreateVertexShader);
		Hook((*ppDevice)->lpVtbl->CreateHullShader, ourCreateHullShader, orig_CreateHullShader);
		Hook((*ppDevice)->lpVtbl->CreateDomainShader, ourCreateDomainShader, orig_CreateDomainShader);
		Hook((*ppDevice)->lpVtbl->CreateGeometryShader, ourCreateGeometryShader, orig_CreateGeometryShader);
		Hook((*ppDevice)->lpVtbl->CreateGeometryShaderWithStreamOutput, ourCreateGeometryShaderWithStreamOutput, orig_CreateGeometryShaderWithStreamOutput);
		Hook((*ppDevice)->lpVtbl->CreatePixelShader, ourCreatePixelShader, orig_CreatePixelShader);

		Hook((*ppDevice)->lpVtbl->CreateShaderResourceView, ourCreateShaderResourceView, orig_CreateShaderResourceView);
		Hook((*ppDevice)->lpVtbl->CreateTexture2D, ourCreateTexture2D, orig_CreateTexture2D);


		//Hook((*ppDevice)->lpVtbl->CreateDeferredContext, ourCreateDeferredContext, orig_CreateDeferredContext);

		/*IDXGIDevice *pDXGIDev = nullptr;
		(*ppDevice)->lpVtbl->QueryInterface(*ppDevice, IID_IDXGIDevice, (void**)&pDXGIDev);
		if (pDXGIDev && ppImmediateContext)
			dxgiDev = pDXGIDev;*/

		ID3D11DeviceContext *immCtx = ppImmediateContext ? *ppImmediateContext : 0;
		/*if (!immCtx)
			(*ppDevice)->lpVtbl->GetImmediateContext(*ppDevice, &immCtx);*/

		if (immCtx)
		{
			updateContextHooks(immCtx->lpVtbl);
			Hook(immCtx->lpVtbl->Map, ourMap, orig_Map);
			Hook(immCtx->lpVtbl->PSSetShaderResources, ourPSSetShaderResources, orig_PSSetShaderResources);
		}

		if(Config::instance().guiEnabled)
			CreateGUI();
	}
	
	return hr;
}

////////////////////////////////////////// DLL Entrypoint

BOOL APIENTRY DllMain(HMODULE hModule,
	DWORD  ul_reason_for_call,
	LPVOID lpReserved
)
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}