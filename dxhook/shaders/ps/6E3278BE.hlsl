SamplerState SS_DEFAULT_s : register(s0);
Texture2DArray<float4> DiffuseTextures : register(t0);
Texture2DArray<float4> DiffuseTexture1 : register(t1);


Texture2DArray<float4> DiffuseTextures1 : register(t99);

float4 main( 
  float4 v0 : SV_POSITION0,
  float2 v1 : TEXCOORD0,
  nointerpolation uint2 v2 : TEXCOORD1,
  nointerpolation float w2 : TEXCOORD2,
  nointerpolation float4 v3 : TEXCOORD3) : SV_Target
{
  float4 r0,r1;
  
  if(v2.x < 512)
    r0 = DiffuseTextures.Sample(SS_DEFAULT_s, float3(v1, v2.x));
  else
    r0 = DiffuseTextures1.Sample(SS_DEFAULT_s, float3(v1, v2.x-512));

  r1 = DiffuseTexture1.Sample(SS_DEFAULT_s, float3(v1, v2.y));
  
  return v3 * lerp(r0, r1, w2);
}