#pragma once

#include <stdint.h>

typedef struct SHA256Context {
  uint32_t state[8];
  uint32_t count[2];
  unsigned char buf[64];
} SHA256_CTX;

extern void SHA256_Transform(uint32_t * state, const unsigned char block[64]);
extern void SHA256_Init(SHA256_CTX * ctx);
extern void SHA256_Update(SHA256_CTX * ctx, const void *in, size_t len);
extern void SHA256_Final(unsigned char digest[32], SHA256_CTX * ctx);